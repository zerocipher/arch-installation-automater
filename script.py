"""
The script that do the entire thing.
"""

import pre_installation
import subprocess
import sys


def use_disk():
    print("Type in the disk to use. Start with th root.")
    disk = {}
    root = input("Enter the disk to be used as /\nThis disk WILL BE FORMATTED\n")
    disk["/"] = {
        "Disk": root,
        "Format": True
    }
    home = input("Enter the disk to be used as /home\nSpecifying the same disk as {root} will use the same disk for / and /home\n")
    format_home = input("Format the disk? Default: NO\nEnter y/N\n")
    disk["/home"] = {
        "Disk": home,
    }
    if format_home.lower().startswith('y'):
        disk["/home"]["Format"] = True
    else:
        if home == root:
            disk["/home"]["Format"] = True
        else:
            disk["/home"]["Format"] = False
    
    swap = input("Enter the disk to be used as swap.\nThis disk WILL BE FORMATTED\n")
    disk["swap"] = {
        "Disk": swap,
        "Format": True
    }

    efi = input("Enter EFI partition.\n")
    format_efi = input("\n\nEFI partition, if exisiting on syustem is NOT to be formatted. If you are creating it, format it.\n\nFormat EFI Partition? Default: NO\nEnter y/N")
    if format_efi.startswith('y'):
        format_efi = True
    else:
        format_efi = False
    disk["efi"] = {
        "Disk": efi,
        "Format": format_efi
    }
    
    print("The script does not support additional disk setup for now.")
    return disk

def set_locale():
    """
    Set the locale.
    """
    locale = input("The locale to be used: ")
    cmd = "loadkeys " + locale
    cmd = cmd.split()
    p = subprocess.check_call(cmd, stderr=sys.stderr, stdin=sys.stdin, stdout=sys.stdout)
    if p.stderr:
        print("[ERROR::LOCALE_INPUT_ERROR]: Encountered error setting locale.\nScript terminated.")
        exit(1)

def set_time():
    tz = input("Timezone: ")
    cmd = "timedatectl set-timezone " + tz
    cmd = cmd.split()
    p = subprocess.check_call(cmd, stderr=sys.stderr, stdin=sys.stdin, stdout=sys.stdout)
    if p.stderr:
        print("[]Encountered error setting timezone. Script terminated.")
        exit(1)
    

def startup():
    is_root = pre_installation.isroot()
    if not is_root:
        print("[ERROR::NO_ROOT]: The script did not get access to root permissions. Ensure you are root.")
        print("The script will now terminate")
        exit(1)
    
    set_locale()

    bm = pre_installation.is_boot_mode_supported()
    
    if bm is None or bm == "64":
        print(f"[ERROR::UNSUPPORTED_BOOT_MODE]: Boot mode is not supported. Got {bm} while reading /sys/firmware/efi/fw_platform_size") 

    host = "archlinux.org"
    is_connected = pre_installation.isConnected(host, 443)
    if not is_connected:
        print(f"[ERROR::NO_NETWORK]: Could not connect to the host: {host}")
        print("The script will now terminate")
        exit(1)

    set_time()

    dsk = use_disk()

    for mp in dsk.keys():
        if mp == "swap":
            cmd = "mkswap " + dsk[mp]["Disk"]
            p = subprocess.check_call(cmd, stderr=sys.stderr, stdin=sys.stdin, stdout=sys.stdout)
            if p.stderr:
                print("[ERROR::MAKE_SWAP]: Encountered error while creating swap.\nScript terminated.")
                exit(1)
            
            cmd = "swapon " + dsk[mp]["Disk"]
            p = subprocess.check_call(cmd, stderr=sys.stderr, stdin=sys.stdin, stdout=sys.stdout)
            if p.stderr:
                print("[ERROR::MAKE_SWAP]: Encountered error while creating swap.\nScript terminated.")
                exit(1)
        elif mp == "efi":
            if dsk[mp]["Format"]:
                cmd = "mkfs.fat " + dsk[mp]["Disk"]
                p = subprocess.check_call(cmd, stderr=sys.stderr, stdin=sys.stdin, stdout=sys.stdout)
                if p.stderr:
                    print("[ERROR::MAKE_PARTITION]: Encountered error setting up efi partition.\nScript terminated.")
                    exit(1)
            else:
                continue
        else:
            if dsk[mp]["Format"]:
                cmd = "mkfs.ext4 " + dsk[mp]["Disk"]
                p = subprocess.check_call(cmd, stderr=sys.stderr, stdin=sys.stdin, stdout=sys.stdout)
                if p.stderr:
                    print("[ERROR::MAKE_PARTITION]: Encountered error setting up partition.\nScript terminated.")
                    exit(1)
            else:
                continue
    
    print("All partition operations exited successfully.\nThe script will now continue to mount the partitions and install system\n")



    
    exit(0)

print(startup())