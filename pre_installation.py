#!/usr/bin/python3


"""
Pre install script that checks if the system is supported.
"""

import os
import socket

def isroot():
    """
    Check if root.
    MUST return true in installer.
    """
    return os.geteuid() == 0

def isConnected(host: str = "1.1.1.1", port: int = 53, timeout: int = 3):
    try:
        socket.setdefaulttimeout(timeout)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect((host, port))
        if sock is not None:
            sock.close
        return True
    except OSError as ose:
        print(ose)
    return False

def is_boot_mode_supported():
    file = "/sys/firmware/efi/fw_platform_size"
    try:
        f = open(file, 'r')
        bm = f.read()
        f.close()

        return str(bm)
    except Exception as e:
        return None
    